﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AspBackEnd.Entities
{
    public class Booking
    {
        public Guid BookingID { set; get; }
        public DateTimeOffset From { get; set; }
        public DateTimeOffset To { get; set; }
    }
}
