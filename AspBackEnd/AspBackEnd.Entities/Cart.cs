﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AspBackEnd.Entities
{
    public class Cart
    {
        public long CartID { set; get; }
        public int Qty { set; get; }
        public Guid CustomerID { set; get; }
        public Guid ProductID { set; get; }
        public Customer Customer { set; get; }
        public Product Product { set; get; }
    }
}
