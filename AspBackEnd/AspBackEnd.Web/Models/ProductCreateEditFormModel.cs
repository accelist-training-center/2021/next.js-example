﻿using Accelist.BlazorRAD;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AspBackEnd.WebApp.Models
{
    public class ProductCreateEditFormModel
    {
        [SmartFormInput(Row = 1)]
        public string Name { set; get; }

        [SmartFormInput(SmartFormInputType.Number, Row = 2)]
        public decimal Price { set; get; }

        [SmartFormInput(SmartFormInputType.DateTime, Row = 3)]
        public DateTimeOffset? StockDate { set; get; }

        [SmartFormInput(SmartFormInputType.Time, Row = 4)]
        public TimeSpan? AvailableAt { set; get; }

        [SmartFormInput(SmartFormInputType.Date, Row = 5)]
        public string ExpirationDate { set; get; }

        [SmartFormInput(SmartFormInputType.Dropdown, Row = 6)]
        public string Category { set; get; }

        [SmartFormInput(SmartFormInputType.RadioGroup, Row = 7)]
        public string Fragile { set; get; }

        [SmartFormInput(SmartFormInputType.CheckboxGroup, Row = 8)]
        public List<SelectOption> Bundle { set; get; }

        [SmartFormInput(SmartFormInputType.Autocomplete, Row = 9)]
        public SelectOption Brand { set; get; }

        [SmartFormInput(SmartFormInputType.Tags, Row = 9)]
        public List<SelectOption> Brand2 { set; get; }

        [SmartFormInput(SmartFormInputType.MultiText, Row = 10)]
        public List<string> Reviews { set; get; }
    }

    public class ProductCreateEditFormValidator : AbstractValidator<ProductCreateEditFormModel>
    {
        public ProductCreateEditFormValidator()
        {
            RuleFor(Q => Q.Name).NotEmpty().MaximumLength(255);
            RuleFor(Q => Q.Price).NotEmpty().LessThan(int.MaxValue);
        }
    }
}
