﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AspBackEnd.WebApp.Models
{
    public class CustomerCreateEditFormModel
    {
        public string Name { set; get; }

        public string Email { set; get; }
    }

    public class CustomerCreateEditFormValidator : AbstractValidator<CustomerCreateEditFormModel>
    {
        public CustomerCreateEditFormValidator()
        {
            RuleFor(Q => Q.Name).Cascade(CascadeMode.Stop).NotEmpty().MaximumLength(255);
            RuleFor(Q => Q.Email).Cascade(CascadeMode.Stop).NotEmpty().MaximumLength(255).EmailAddress();
        }
    }
}
