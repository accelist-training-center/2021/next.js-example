﻿using AspBackEnd.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace AspBackEnd.WebApp.API.StudentManagement
{
    [Route("api/[controller]")]
    [ApiController]
    public class BookingController : ControllerBase
    {
        public BookingController(ShopDbContext shopDbContext)
        {
            this.DB = shopDbContext;
        }

        public ShopDbContext DB { get; }

        // GET: api/<BookingController>
        [HttpGet]
        public async Task<ActionResult<List<BookingListItem>>> Get()
        {
            //var bookings = new List<BookingListItem>();
            //bookings.Add(new BookingListItem
            //{
            //    BookingID = new Guid("A196BDB3-D59B-4A16-9CD9-9D28FC34C1C1"),
            //    From = DateTimeOffset.UtcNow.AddDays(-7),
            //    To = DateTimeOffset.UtcNow.AddDays(3)
            //});

            var items = await DB.Bookings.AsNoTracking().Select(Q => new BookingListItem
            {
                BookingID = Q.BookingID,
                From = Q.From,
                To = Q.To
            }).ToListAsync();
            return items;
        }

        // POST api/<BookingController>
        [HttpPost]
        public async Task<ActionResult<bool>> Post([FromBody] BookingPostRequestModel model)
        {
            DB.Bookings.Add(new Booking
            {
                BookingID = Guid.NewGuid(),
                From = model.From.ToUniversalTime(),
                To = model.To.ToUniversalTime()
            });
            await DB.SaveChangesAsync();
            return true;
        }
    }

    public class BookingPostRequestModel
    {
        [Required]
        public DateTimeOffset From { set; get; } // ISO 8601

        [Required]
        public DateTimeOffset To { set; get; }
    }

    public class BookingListItem
    {
        public Guid BookingID { set; get; }

        public DateTimeOffset From { set; get; }

        public DateTimeOffset To { set; get; }
    }
}
