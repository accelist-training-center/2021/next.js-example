﻿using AspBackEnd.WebApp.Services;
using IdentityModel.Client;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace AspBackEnd.WebApp.API
{
    [Route("api/[controller]")]
    [ApiController]
    public class CartController : ControllerBase
    {
        public CartController(CartService cartService)
        {
            this.CartSvc = cartService;
        }

        public CartService CartSvc { get; }

        [HttpGet("token")]
        public async Task<ActionResult<string>> GetTokenAsync()
        {
            // Client ID dan Client Secret seperti username dan password si app non-human ini
            var clientID = "machine-app";
            var clientSecret = "d46b5ede-595f-443b-a537-560f56998400";

            // Pertama" kita request informasi tentang Authentication Servernya (via Discovery URL)
            var client = new HttpClient();
            var disco = await client.GetDiscoveryDocumentAsync("https://sso.accelist.com/auth/realms/Dev");

            // Kedua, kita request token, minta access ke customer-api
            var response = await client.RequestClientCredentialsTokenAsync(new ClientCredentialsTokenRequest
            {
                Address = disco.TokenEndpoint,
                ClientId = clientID,
                ClientSecret = clientSecret,
                Scope = "customer-api",         // anggep aja scope = API apa yang mau diakses
            });

            // Kalau berhasil, dapet access token, bisa dicek di https://jwt.io/
            // Please jangan ngepaste token ke sembarang tempat...
            Console.WriteLine(response.AccessToken);

            // Kita mau pake tokennya untuk access customer API
            var request = new HttpRequestMessage(HttpMethod.Get, "http://localhost:58778/api/customer");
            request.SetBearerToken(response.AccessToken);
            
            // Fire!
            var customer = await client.SendAsync(request);

            // dapet string dalam bentuk JSON
            var content = await customer.Content.ReadAsStringAsync();
            Console.WriteLine(content);

            return Ok(content);
        }

        [HttpPost("read")]
        public async Task<ActionResult<List<CartListItem>>> ReadCart([FromBody] GetCartModel value)
        {
            if (ModelState.IsValid == false)
            {
                return BadRequest(ModelState);
            }

            return await CartSvc.GetCartForCustomer(value.CustomerID);
        }

        [HttpPost("add")]
        public async Task<ActionResult<bool>> AddToCart([FromBody] AddToCartModel value)
        {
            if (await CartSvc.HasCustomerID(value.CustomerID) == false)
            {
                ModelState.AddModelError("CustomerID", "Customer does not exists!");
            }

            if (await CartSvc.HasProductID(value.ProductID) == false)
            {
                ModelState.AddModelError("ProductID", "Product does not exists!");
            }

            if (ModelState.IsValid == false)
            {
                return BadRequest(ModelState);
            }

            await CartSvc.AddToCart(value.CustomerID, value.ProductID, value.Qty);
            return true;
        }

        [HttpPost("update")]
        public async Task<ActionResult<bool>> UpdateCart([FromBody] UpdateCartModel value)
        {
            if (await CartSvc.HasCartID(value.CartID) == false)
            {
                ModelState.AddModelError("CartID", "Item in cart does not exists!");
            }

            if (ModelState.IsValid == false)
            {
                return BadRequest(ModelState);
            }

            await CartSvc.UpdateCart(value.CartID, value.Qty);
            return true;
        }

        [HttpPost("remove")]
        public async Task<ActionResult<bool>> RemoveItemInCart([FromBody] RemoveItemInCartModel value)
        {
            if (await CartSvc.HasCartID(value.CartID) == false)
            {
                ModelState.AddModelError("CartID", "Item in cart does not exists!");
            }

            if (ModelState.IsValid == false)
            {
                return BadRequest(ModelState);
            }

            await CartSvc.RemoveItemInCart(value.CartID);
            return true;
        }

        #region Models
        public class GetCartModel
        {
            [Required]
            public Guid CustomerID { set; get; }
        }

        public class CartListItem
        {
            public long CartID { set; get; }

            public Guid ProductID { set; get; }

            public string ProductName { set; get; }

            public decimal Price { set; get; }

            public int Qty { set; get; }
        }

        public class AddToCartModel
        {
            [Required]
            public Guid CustomerID { set; get; }

            [Required]
            public Guid ProductID { set; get; }

            [Required]
            [Range(1, 1000)]
            public int Qty { set; get; }
        }

        public class UpdateCartModel
        {
            [Required]
            public long CartID { set; get; }

            [Required]
            [Range(1, 1000)]
            public int Qty { set; get; }
        }

        public class RemoveItemInCartModel
        {
            [Required]
            public long CartID { set; get; }
        }
        #endregion
    }
}
