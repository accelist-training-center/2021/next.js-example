﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AspBackEnd.WebApp.Services
{
    public class MathService : IMathService
    {
        public int Add(int a, int b)
        {
            return a + b;
        }
    }
}
