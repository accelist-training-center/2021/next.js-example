﻿namespace AspBackEnd.WebApp.Services
{
    public interface IMathService
    {
        int Add(int a, int b);
    }
}