using AspBackEnd.WebApp.Data;
using AspBackEnd.Entities;
using AspBackEnd.WebApp.Models;
using AspBackEnd.WebApp.Services;
using FluentValidation;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using NSwag.Generation.Processors.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace AspBackEnd.WebApp
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddRazorPages();
            services.AddServerSideBlazor();
            services.AddSingleton<WeatherForecastService>();

            services.AddScoped<IMathService, MathService>();

            services.Configure<AppSettings>(Configuration);
            services.AddScoped<AppSettings>(di => di.GetRequiredService<IOptionsMonitor<AppSettings>>().CurrentValue);

            // ini untuk web API...
            services.AddDbContextPool<ShopDbContext>(options =>
            {
                options.UseSqlite(Configuration.GetConnectionString("ShopDB"));
            });

            // ini untuk Blazor...
            services.AddPooledDbContextFactory<ShopDbContext>(options =>
            {
                options.UseSqlite(Configuration.GetConnectionString("ShopDB"));
            });

            //services.AddSwaggerGen(options =>
            //{
            //    options.AddSecurityDefinition("Bearer", new Microsoft.OpenApi.Models.OpenApiSecurityScheme
            //    {
            //        In = Microsoft.OpenApi.Models.ParameterLocation.Header,
            //        Description = "Please insert JWT with Bearer into field",
            //        Name = "Authorization",
            //        Type = Microsoft.OpenApi.Models.SecuritySchemeType.ApiKey
            //    });
            //    options.OperationFilter<AuthorizeCheckOperationFilter>();
            //});

            services.AddSwaggerDocument(options =>
            {
                options.AddSecurity("Bearer", new NSwag.OpenApiSecurityScheme
                {
                    Type = NSwag.OpenApiSecuritySchemeType.ApiKey,
                    In = NSwag.OpenApiSecurityApiKeyLocation.Header,
                    Name = "Authorization",
                    Description = "Type into the textbox: Bearer {your JWT token}."
                });
            });


            services.AddScoped<CartService>();

            services.AddAuthentication(options =>
            {
                options.DefaultScheme = "cookie";
                options.DefaultChallengeScheme = "oidc";
            }).AddCookie("cookie", options =>
            {
                options.LoginPath = "/account/login";
            }).AddOpenIdConnect("oidc", options =>
            {
                // https://www.thinktecture.com/en/identity/samesite/prepare-your-identityserver/
                options.CorrelationCookie.SameSite = Microsoft.AspNetCore.Http.SameSiteMode.Unspecified;
                options.NonceCookie.SameSite = Microsoft.AspNetCore.Http.SameSiteMode.Unspecified;

                options.Authority = "https://sso.accelist.com/auth/realms/Dev";
                options.ClientId = "aspnet-training";
                options.ResponseType = "code";
                options.Scope.Add("email");
                options.UsePkce = true;
                options.GetClaimsFromUserInfoEndpoint = true;
                options.ClaimActions.MapAll();
                options.ClaimActions.MapJsonKey(ClaimTypes.Name, "name");
                options.ClaimActions.MapJsonKey(ClaimTypes.Email, "email");
            }).AddJwtBearer("customer-api", options =>
             {
                 options.Authority = "https://sso.accelist.com/auth/realms/Dev";
                 options.Audience = "customer-api";
             });

            /*
             
             */

            services.AddValidatorsFromAssemblyContaining<Startup>();
            //services.AddTransient<IValidator<CustomerCreateEditForm>, CustomerCreateEditFormValidator>();

            services.AddCors(option =>
            {
                option.AddPolicy("BelajarNextJS", policy =>
                {
                    policy.WithOrigins("http://localhost:3000")
                        .AllowAnyHeader()
                        .AllowAnyMethod();
                });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            // DO NOT DO THIS IN PRODUCTION!!!
            using (var scope = app.ApplicationServices.CreateScope())
            {
                var db = scope.ServiceProvider.GetRequiredService<ShopDbContext>();
                db.Database.EnsureCreated();
            }


            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
            }

            app.UseStaticFiles();

            app.UseOpenApi();
            //app.UseSwaggerUi3(config => 
            //{
            //    config.OAuth2Client = new NSwag.AspNetCore.OAuth2ClientSettings
            //    {
            //        ClientId = "aspnet-training",
            //        AppName = "Test",
            //        UsePkceWithAuthorizationCodeGrant = true
            //    };
            //});

            app.UseSwaggerUi3();

            app.UseRouting();

            app.UseCors("BelajarNextJS");

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapDefaultControllerRoute();
                endpoints.MapBlazorHub();
                endpoints.MapFallbackToPage("/_Host");
            });
        }
    }
}
